 $("#city").chosen({max_selected_options: 10})
 $('#unit').chosen({disable_search_threshold: 10});
 
 var myLineChart = null
 
 var colorsFree = ["#00ad5f", "#ad6e00", "#ad007f", "#5BC8AC", "#E6D72A", "#F18D9E","#D09683","#07575B","#E4EA8C", "#4D85BD"]
 
 var citiesColors = []
 
 var pointsFree = ['#187047', '#7d5818', '#731759',"#4b8274","#968d20","#ba808a", "#917870","#0c494d","#c5c989","#4c6d8f"]

 var unitLabel = "temperature °C";
 var timeLabel = "hours";
 
 var activeUnit= "temp";
 var activeTime = "daily"

 function hexToRgb(hex) {
	 	hex = hex.replace(/[^0-9A-F]/gi, '');
	    var bigint = parseInt(hex, 16);
	    var r = (bigint >> 16) & 255;
	    var g = (bigint >> 8) & 255;
	    var b = bigint & 255;
	
	    return r + "," + g + "," + b;
	}
 
 function findColor(name) {
	 var length = citiesColors.length
	 for (var i = 0; i < length; i++) {
		 	if (citiesColors[i].name == name) {
		 		cityColor = citiesColors[i]
		 		break
		 	}
	 }
	 return cityColor
 }

$(document).ready(function(){ 
	
	 $('#ModalBase').on('show.bs.modal', function (event) {
		 var cityCard = $(event.relatedTarget)
		 var cityName = cityCard.data('city')
	     
	     $.ajax({
			type : "POST",
			async: false,
			url : "/apiRequests/sendRequest/?selected="+cityName,
			contentType: "application/json",
			success : function(data){
				 if($('#weekly').hasClass('active-button')) {
					 // treba da se promeni klasa kontejnera na dane
				      fillModalDays(cityName, data)
				    } else {
				    	// ne treba da se promeni klasa jer je ova bazna
				      fillModalHours(cityName, data)
				    }			
				 },
			error : function(data) {
				alert(data.responseText)
			}
		 });
	 });
	 
	 $('#weatherButton').on('click', function() {
			 if ($("#city").val().length != 0) {
				 change()
			 }
		 })
	 
	 $('#city').on('change', function(evt, params) {
		// necemo zvati change jer samo dodajemo i oduzimamo gradove
		 // ne moramo sve da crtamo od pocetka
		 // moramo samo kada menjamo unit ili button
		 // ovako ostaje ista pozadina, samo dodajemo ili oduzimamo dataset
		 //change()
		 if (params.selected) {
			 // izvrsio je selekciju
			 addDataset(params)
		 } else {
			 // izvrsio je deselekciju
			 removeDataset(params)
		 }
	})
	 
	 /* Unit Buttons */
	 $('.radio-button-unit').on('click', function () {
		 if ($("#city").val().length != 0) {
			 document.body.style.cursor='wait';
		 }
	     $('.radio-button-unit').removeClass('active-button');
	     $(this).addClass('active-button');
	     activeUnit = $(this).attr('id')
	     change()
	 });
	 
	 /* Forecast type Buttons */
	 $('.radio-button-type').on('click', function () {
		 if ($("#city").val().length != 0) {
			 document.body.style.cursor='wait';
		 }
	     $('.radio-button-type').removeClass('active-button');
	     $(this).addClass('active-button');
	     activeTime = $(this).attr('id')
	     change()
	 });
	 
	 
	 $("#daily").addClass('active-button')
	 
	 

 })
 
 function fillModalDays(cityName, data){
	 

	 var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu','Fri','Sat']
		
	 var dt = new Date();
	 var day = dt.getDay()
	 var color = findColor(cityName)
	 
	 // menjanje klase kontejnera
	 $("#modal-container").removeClass("m-container-hours").addClass("m-container-days");
	 
	 $(".color-card").css('background', 'url(images/grey.jpg), rgb('+hexToRgb(color.color)+', 0.5)')
	 $(".color-card").css('background-blend-mode', 'multiply')
	 $(".color-card").css('border-color', color.point)
	 // menjanje zakrivljenosti ivice
	 $("#m-main7").css('border-radius', '0 0 20px 0')
	 $("#m-title").text(cityName)
	 $("#m-w-icon").attr("src", "images//"+data.current.weather[0].main+".png")
	 $("#m-temp").text("Current Temp: "+Math.round(data.current.temp)+"°C")
	 $("#m-description").text(data.current.weather[0].main)
	 
	 var k;
	 for ( var j = 0; j < 7; j++ ) {
		 	k = j+1;
		 	$("#m-date-time" + k).text(days[day])
		 	$("#m-main-min-temp"+j).text("MinTemp:")
		 	$("#m-main-max-temp"+j).text("MaxTemp:")
		 	$("#m-main-w-icon" + k).attr("src", "images//"+data.daily[j].weather[0].main+".png")
		    $("#d-min-temp"+k).text(Math.round(data.daily[j].temp.min) + "°C")
		    $("#d-max-temp"+k).text(Math.round(data.daily[j].temp.max) + "°C")
		    $("#d-pressure" + k).text(Math.round(data.daily[j].pressure) +"hPa")
		    $("#d-humidity" + k).text(Math.round(data.daily[j].humidity) +"%")
		    $("#d-wind" + k).text(Math.round(data.daily[j].wind_speed) +"m/s")
		    day++
		    day = day%7
	 }
	 $("#m-date-time8").hide();
	 $("#m-date-time9").hide();
	 $("#m-main8").hide();
	 $("#m-main9").hide();
	 
 }
 
 function fillModalHours(cityName, data){

	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var color = findColor(cityName)
	 
	 // menjanje klase kontejnera
	 $("#modal-container").removeClass("m-container-days").addClass("m-container-hours");
	 $("#m-date-time8").show();
	 $("#m-date-time9").show();
	 $("#m-main8").show();
	 $("#m-main9").show();
	 // menjanje zakrivljenosti ivice
	 $("#m-main7").css('border-radius', '0')
	 
	 
	 $(".color-card").css('background', 'url(images/grey.jpg), rgb('+hexToRgb(color.color)+', 0.5)')
	 $(".color-card").css('background-blend-mode', 'multiply')
	 $(".color-card").css('border-color', color.point)
	 $("#m-title").text(cityName)
	 $("#m-w-icon").attr("src", "images//"+data.current.weather[0].main+".png")
	 $("#m-temp").text("Current Temp: "+Math.round(data.current.temp)+"°C")
	 $("#m-description").text(data.current.weather[0].main)
	 
	 var i = 0;
	 
	 for ( var j = 1; j < 10; j++ ) {
		 	$("#m-date-time"+j).text(hours.toString()+":00")
		 	$("#m-main-min-temp"+j).text("Temperature:")
		 	$("#m-main-max-temp"+j).text("Feels like:")
		 	$("#m-main-w-icon" + j).attr("src", "images//"+data.hourly[i].weather[0].main+".png")
		    $("#d-min-temp"+j).text(Math.round(data.hourly[i].temp) + "°C")
		    $("#d-max-temp"+j).text(Math.round(data.hourly[i].feels_like) + "°C")
		    $("#d-pressure" + j).text(Math.round(data.hourly[i].pressure) +"hPa")
		    $("#d-humidity" + j).text(Math.round(data.hourly[i].humidity) +"%")
		    $("#d-wind" + j).text(Math.round(data.hourly[i].wind_speed) +"m/s")
		    if($("#daily").hasClass('active-button')){
		    	hours = hours+3
		    	i = i+3
		    }else{
		    	if(i==42){
		    		hours = hours + 6
		    		i = i + 5
		    	}else{
		    		hours = hours+6
			    	i = i+6
		    	}
		    }
		    hours = hours%24
	 }
     
 }
 
 function change(){
	 
	 removeData()
	 
	 if(activeTime == "daily" || activeTime== "twoDays"){
		 timeLabel = "hours"
	 }
	 else{
		 timeLabel = "days"
	 }
	 
	 if(activeUnit == "temp"){
		 unitLabel = "temperature °C" 
	 }else if(activeUnit == "pressure"){
		 unitLabel = "air pressure hPa"
	 }else if(activeUnit == "wind"){
		 unitLabel = "wind speed m/s"
	 }else{
		 unitLabel = "humidity %"
	 }
	 
	 if ($("#city").val().length != 0) {
		 makeGraph()
	 }
	 
	 var selected = $("#city").val()
	 for ( var i = 0; i < selected.length; i++ ) {
		 var name = selected[i]
		 $.ajax({
			type : "POST",
			async: false,
			url : "/apiRequests/sendRequest/?selected="+name,
			contentType: "application/json",
			success : function(data){
				currentWeather(data, name)
				if( activeUnit == "temp"){
					 temperature(data, name)
				 } else if( activeUnit == "pressure"){
					 airPressure(data, name)
				 } else if( activeUnit == "wind"){
					 windSpeed(data, name)
				 }else{
					 humidity(data, name)
				 }
			},
			error : function(data) {
				alert(data.responseText)
			}
		 });
	 }
	 
 }
 
 /*Current weather*/
 function currentWeather(data, name){
	 var charts = $("#charts")
	 
	 var container = $("<div class='table-container' id='"+name.replace(/\s/g, '')+ "' data-toggle='modal' data-target='#ModalBase' data-city='"+name+"'></div>")
	 
	 container.append("<div class='city-name'><span id='city-name'>"+name+"</span></div>")
	 container.append("<div class='weather-icon'><img class='w-icon' src='images//"+data.current.weather[0].main+".png'></div>")
	 container.append("<div class='current-temp'><img class='c-icon' src='images/current-temp.png'><span class='c-data' id='current-temp'>&nbsp;"+Math.round(data.current.temp)+"°C</span></div>")
	 container.append("<div class='min-temp'><img class='icon' src='images/min-temp.png'><span class='data' id='min-temp'>&nbsp;"+Math.round(data.daily[0].temp.min)+"°C</span></div>")
	 container.append("<div class='max-temp'><img class='icon' src='images/max-temp.png'><span class='data' id='max-temp'>&nbsp;"+Math.round(data.daily[0].temp.max)+"°C</span></div>")
	 container.append("<div class='air-pressure'><img class='icon' src='images/pressure.png'><span id='air-pressure'>&nbsp;"+Math.round(data.current.pressure)+"hPa</span></div>")
	 container.append("<div class='weather-description'><span id='weather-description'>"+data.current.weather[0].main+"</span></div>")
	 container.append("<div class='humidity'><img class='icon' src='images/humidity.png'><span class='data' id='humidity'>&nbsp;"+Math.round(data.current.humidity)+ "%</span></div>")
	 container.append("<div class='wind'><img class='icon' src='images/wind.png'><span class='data' id='wind'>&nbsp;"+Math.round(data.current.wind_speed)+"m/s</span></div>") 
 
	 // boja
	 // nadje prvo boju
	 var color = findColor(name).color
	 container.css('backgroundColor', "rgb("+hexToRgb(color)+",0.5)")
	 
	 charts.append(container)
 }
 
 /*Make graph for all units */
 function makeGraph(){
	 
	 var ctx = $('#myChart');
	 myLineChart = new Chart(ctx, {
	    type: 'line',
	    options: {
	    	tooltips: {
	    		mode: 'index',
	    		intersect: false,
	    	},
		    scales: {
		        yAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: unitLabel
		          }
		        }],
		        xAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: timeLabel
		          }
			    }]
		    }
	    }   
	 });
 }
 
 /*Temperature Graphs*/
 function temperature(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphTemp(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphTemp(data, name)
	 }else{
		 weeklyGraphTemp(data, name)
	 }
 }
 
 function dailyGraphTemp(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 27; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].temp))
		    j = j+2
		    hours = hours + 3
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name) 
}
 
 function twoDaysGraphTemp(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].temp))
		    if(j==42){
		    	j = j + 4
		    }else{
		    	j = j + 5
		    }
		    hours = hours + 6
		    hours = hours % 24
		    
	 }
	 addData(myLineChart, time, values, name) 
} 
 
 function weeklyGraphTemp(data, name){
	
	 var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu','Fri','Sat']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].temp.day))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}
 
 
 /*Air pressure Graphs*/
 function airPressure(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphAirPress(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphAirPress(data, name)
	 }else{
		 weeklyGraphAirPress(data, name)
	 }
 }

 function dailyGraphAirPress(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 27; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].pressure))
		    j = j+2
		    hours = hours + 3
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}
 
 function twoDaysGraphAirPress(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].pressure))
		    if(j==42){
		    	j = j + 4
		    }else{
		    	j = j + 5
		    }
		    hours = hours + 6
		    hours = hours % 24
	 }
	 addData(myLineChart, time, values, name) 
} 
 
function weeklyGraphAirPress(data, name){
	
	var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu','Fri','Sat']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].pressure))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}
 

/*Wind speed Graphs*/
function windSpeed(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphWindSpeed(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphWindSpeed(data, name)
	 }else{
		 weeklyGraphWindSpeed(data, name)
	 }
}

function dailyGraphWindSpeed(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 27; j++ ) {
		 	time.push(hours);
		    values.push(Math.round(data.hourly[j].wind_speed))
		    j = j+2
		    hours = hours + 3
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}

function twoDaysGraphWindSpeed(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].wind_speed))
		    if(j==42){
		    	j = j + 4
		    }else{
		    	j = j + 5
		    }
		    hours = hours + 6
		    hours = hours % 24
	 }
	 addData(myLineChart, time, values, name) 
} 

function weeklyGraphWindSpeed(data, name){
	
	var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu','Fri','Sat']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].wind_speed))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}


/*Humidity Graphs*/
function humidity(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphHumidity(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphHumidity(data, name)
	 }else{
		 weeklyGraphHumidity(data, name)
	 }
}

function dailyGraphHumidity(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 27; j++ ) {
		 	time.push(hours);
		    values.push(Math.round(data.hourly[j].humidity))
		    j = j+2
		    hours = hours + 3
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}

function twoDaysGraphHumidity(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].humidity))
		    if(j==42){
		    	j = j + 4
		    }else{
		    	j = j + 5
		    }
		    hours = hours + 6
		    hours = hours % 24
	 }
	 addData(myLineChart, time, values, name) 
} 

function weeklyGraphHumidity(data, name){
	
	var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu','Fri','Sat']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].humidity))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}

 
 function addData(chart, labels, data, name) {
	 
	 chart.data.labels = labels
	 
	 var cityColor = findColor(name)
	 
	 var color = hexToRgb(cityColor.color)
	 
	 chart.data.datasets.push({
		 label: cityColor.name,
		 // boja linije i labele
		 backgroundColor: "rgb("+color+", 0.1)",
		 borderColor: cityColor.color,
		 pointHoverRadius: 5,
		 // boja tacaka
		 pointBackgroundColor: cityColor.point,
		 pointBorderColor: cityColor.point,
		 // samo linija
		 fill: 'start',
		 data: data
	 })

	 chart.update()

	 document.body.style.cursor='default';
}
 
 function removeData(){
	 
	 $("#charts").html('')
	 $("#charts").append("<label class='note-label'>* Click on card for detailed view</label>")
	 $("#myChart").remove()
	 
	 /*$("#mainChart").append("<div id='charts' class='charts-tables tables'></div>")
	 
	 $("#mainChart").append("<div id = 'mainDiv' class='charts-tables charts'd></div>")
	 
	 
	
	 $("#mainDiv").append("<button id='temp' class='radio-button' type='button'>Temperature</button>")
	 $("#mainDiv").append("<button id='pressure' class='radio-button' type='button'>Air Pressure</button>")
	 $("#mainDiv").append("<button id='wind' class='radio-button' type='button'>Wind speed</button>")
	 $("#mainDiv").append("<button id='humidity' class='radio-button' type='button'>Humidity</button>")*/
	 	
	 myLineChart = null
	 $("#mainDiv").append("<canvas id=\"myChart\" width=\"753\" height=\"350\"></canvas>")
	
 }
 
 function addDataset(params) {
	 // on ce samo da kreira objekat colorCity
	 // i da ga u ubaci u listu svih gradova
	 // kada se promeni nesto, pozove change()
	 // onda ce da se uzmu samo svi objekti iz liste colorsCities
	 // i da se iscrtaju
	 // change iterira kroz njih sve
	 var color = colorsFree.pop()
	 var point = pointsFree.pop()
	 var cityColor = {
			 name: params.selected,
			 color: color,
			 point: point
	 }
	 citiesColors.push(cityColor)
 }
 
 function removeDataset(params) {
	 var targetLabel = params.deselected
	 
	 // brisanje iz citiesColor
	 var index = citiesColors.findIndex(i => i.name === targetLabel);
	 var removed = citiesColors.splice(index, 1);
	 
	 // ako smo ikada dodali dataset radimo ovo ispod
	 // ako ne, vristi na nas
	 if (myLineChart) {
		 if (myLineChart.data) {
		 
			 // oslobadjanje boje
			 // splice vrati listu, pa moramo da indeksiramo
			 colorsFree.push(removed[0].color)
			 pointsFree.push(removed[0].point)
			 
			 // brisanje iz chart
			 // Filter out and set back into chart.data.datasets
			 myLineChart.data.datasets = myLineChart.data.datasets.filter(function(obj) {
			     return (obj.label != targetLabel); 
			 });
			 // Repaint
			 myLineChart.update();
			 
			 // brisanje kartice
			 $('#'+targetLabel.replace(/\s/g, '')).remove()
		 }
	 }
	 
	 if ($("#city").val().length == 0) {
		 removeData()
	 }
 }
 
 
	 


