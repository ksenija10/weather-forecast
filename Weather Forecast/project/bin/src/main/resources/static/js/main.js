 $("#city").chosen({max_selected_options: 3})
 $('#unit').chosen({disable_search_threshold: 10});
 
 var myLineChart = null

 var colors = ["#00ad5f", "#ad6e00", "#ad007f"]
 var colorsFree = ["#00ad5f", "#ad6e00", "#ad007f"]
 var colorsUsed = []
 
 var points = ['#187047', '#7d5818', '#731759']
 var pointsFree = ['#187047', '#7d5818', '#731759']
 var pointsUsed = []
 
 $(document).ready(function(){
	 $("#city").bind("chosen:maxselected",
		function() {
			 alert('Maximum number of cities selected!');
	 });
	 
	 $('#city').on('change', function() {
		change()
	 })
	 
	 $('#unit').on('change', function() {
		change()
	 })
	 
	 /* Buttons */
	 $('.radio-button').on('click', function () {
	     $('.radio-button').removeClass('active-button');
	     $(this).addClass('active-button');
	 });
	 
	 $("#daily").addClass('active-button')
	 
	 $( "#daily" ).click(function() {
		 change()
	 });
	 
	 $( "#twoDays" ).click(function() {
		 change()
	 });
	 
	 $( "#weekly" ).click(function() {
		 change()
	 });
	 
	 $('.timepicker').timepicker({
		  timeFormat: 'H:mm',
		  interval: 60,
		  minTime: '00:00',
		  maxTime: '23:00',
		  defaultTime: '10:00',
		  startTime: '10:00',
		      dynamic: false,
		      dropdown: true,
		      scrollbar: true
	}); 
 })
 
 function change(){
	 var unit = $("#unit").val()
	 
	 if( unit == "temp"){
		 removeData()
		 makeTempGraph()
	 } else if( unit == "pressure"){
		 removeData()
		 makeAirPressGraph()
	 } else if( unit == "windSpeed"){
		 removeData()
		 makeWindSpeedGraph()
	 }else{
		 removeData()
		 makeHumidityGraph()
	 }
	 
	 var selected = $("#city").val()
	 for ( var i = 0; i < selected.length; i++ ) {
		 var name = selected[i]
		 $.ajax({
			type : "POST",
			async: false,
			url : "/apiRequests/sendRequest/?selected="+name,
			contentType: "application/json",
			success : function(data){ 
				if( unit == "temp"){
					 temperature(data, name)
				 } else if( unit == "pressure"){
					 airPressure(data, name)
				 } else if( unit == "windSpeed"){
					 windSpeed(data, name)
				 }else{
					 humidity(data, name)
				 }
			},
			error : function(data) {
				alert(data.responseText)
			}
		 });
	 }
	 
 }
 
 /*Temperature Graphs*/
 function temperature(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphTemp(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphTemp(data, name)
	 }else{
		 weeklyGraphTemp(data, name)
	 }
 }
 
 function makeTempGraph(){
	 var ctx = $('#myChart');
	 myLineChart = new Chart(ctx, {
	    type: 'line',
	    options: {
	    	tooltips: {
	    		mode: 'index',
	    		intersect: false,
	    	},
		    scales: {
		        yAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'temperature °C'
		          }
		        }],
		        xAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'time'
		          }
			    }]
		    }
	    }   
	 });
 }
 
 function dailyGraphTemp(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 24; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].temp))
		    hours++
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name) 
}
 
 
 
 function twoDaysGraphTemp(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].temp))
		    hours++
		    hours++
		    hours = hours % 24
		    j++
	 }
	 addData(myLineChart, time, values, name) 
} 
 
 function weeklyGraphTemp(data, name){
	
	var days = ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].temp.day))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}
 
 
 /*Air pressure Graphs*/
 function airPressure(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphAirPress(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphAirPress(data, name)
	 }else{
		 weeklyGraphAirPress(data, name)
	 }
 }
 
 function makeAirPressGraph(){
	 var ctx = $('#myChart');
	 myLineChart = new Chart(ctx, {
	    type: 'line',
	    options: {
	    	tooltips: {
	    		mode: 'index',
	    		intersect: false,
	    	},
		    scales: {
		        yAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'air pressure Pa'
		          }
		        }],
		        xAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'time'
		          }
			    }]
		    }
	    }   
	 });
 }
 
 function dailyGraphAirPress(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 24; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].pressure))
		    hours++
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}
 
 function twoDaysGraphAirPress(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].pressure))
		    hours++
		    hours++
		    hours = hours % 24
		    j++
	 }
	 addData(myLineChart, time, values, name) 
} 
 
function weeklyGraphAirPress(data, name){
	
	var days = ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].pressure))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}
 

/*Wind speed Graphs*/
function windSpeed(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphWindSpeed(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphWindSpeed(data, name)
	 }else{
		 weeklyGraphWindSpeed(data, name)
	 }
}

function makeWindSpeedGraph(){
	 var ctx = $('#myChart');
	 myLineChart = new Chart(ctx, {
	    type: 'line',
	    options: {
	    	tooltips: {
	    		mode: 'index',
	    		intersect: false,
	    	},
		    scales: {
		        yAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'wind speed m/sec'
		          }
		        }],
		        xAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'time'
		          }
			    }]
		    }
	    }   
	 });
}

function dailyGraphWindSpeed(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 24; j++ ) {
		 	time.push(hours);
		    values.push(Math.round(data.hourly[j].wind_speed))
		    hours++
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}

function twoDaysGraphWindSpeed(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].wind_speed))
		    hours++
		    hours++
		    hours = hours % 24
		    j++
	 }
	 addData(myLineChart, time, values, name) 
} 

function weeklyGraphWindSpeed(data, name){
	
	var days = ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].wind_speed))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}


/*Humidity Graphs*/
function humidity(data, name){
	 if($("#daily").hasClass("active-button")){
		 dailyGraphHumidity(data, name)
	 } else if($("#twoDays").hasClass("active-button")){
		 twoDaysGraphHumidity(data, name)
	 }else{
		 weeklyGraphHumidity(data, name)
	 }
}

function makeHumidityGraph(){
	 var ctx = $('#myChart');
	 myLineChart = new Chart(ctx, {
	    type: 'line',
	    options: {
	    	tooltips: {
	    		mode: 'index',
	    		intersect: false,
	    	},
		    scales: {
		        yAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'humidity %'
		          }
		        }],
		        xAxes: [{
		          scaleLabel: {
		            display: true,
		            labelString: 'time'
		          }
			    }]
		    }
	    }   
	 });
}

function dailyGraphHumidity(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 24; j++ ) {
		 	time.push(hours);
		    values.push(Math.round(data.hourly[j].humidity))
		    hours++
		    hours = hours % 24
	 }
	 
	 addData(myLineChart, time, values, name)
}

function twoDaysGraphHumidity(data, name){
	 
	 var dt = new Date();
	 var hours = dt.getHours()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 48; j++ ) {
		 	time.push(hours.toString()+":00");
		    values.push(Math.round(data.hourly[j].humidity))
		    hours++
		    hours++
		    hours = hours % 24
		    j++
	 }
	 addData(myLineChart, time, values, name) 
} 

function weeklyGraphHumidity(data, name){
	
	var days = ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun']
	
	 var dt = new Date();
	 var day = dt.getDay()
	 
	 var time = []
	 var values = []
	 
	 for ( var j = 0; j < 7; j++ ) {
		 	time.push(days[day]);
		    values.push(Math.round(data.daily[j].humidity))
		    day++
		    day = day%7
	 }
	 addData(myLineChart, time, values, name) 
}

 
 function addData(chart, labels, data, name) {
	 
	 chart.data.labels = labels
	 
	 var color = colorsFree.pop()
	 colorsUsed.push(color)
	 var point = pointsFree.pop()
	 pointsUsed.push(point)
	
	 chart.data.datasets.push({
		 label: name,
		 // boja linije i labele
		 backgroundColor: color,
		 borderColor: color,
		 pointHoverRadius: 5,
		 // boja tacaka
		 pointBackgroundColor: point,
		 pointBorderColor: point,
		 // samo linija
		 fill: false,
		 data: data
	 })
	 chart.update()
}
 
 function removeData(){
	 myLineChart = null
	 $("canvas").remove()
	 	
	 $("#mainDiv").append("<canvas id=\"myChart\" width=\"400\" height=\"170\"></canvas>")
	 
	 colorsFree = Array.from(colors)
	 colorsUsed = []
	 
	 pointsFree = Array.from(points)
	 pointsUsed = []
 }
 
 
	 


