# Weather Forecast

Weather Forecast web project for HCI by Ksenija Prcic sw10/17

Korišćene tehnologije: Spring + JQuery
Korišćene biblioteke: Bootstrap, Chose, Chart.js, org.json, org.springframework.boot

Pokretanje projekta:
1) Importovati projekat u Eclipse EE workspace: File-> Import -> Maven -> Existing Maven Project i odabrati projekat
2) Instalirati sve dependency-je iz pom.xml: Run as -> Maven Builds i nakon otvaranja prozora u Goals upisati install i zatim kliknuti dugme Run
3) Pokretanje rada programa: desni klik na ProjectApplication(project/src/main/java/hci.project/ProjectApplication) -> Run as -> Java Application
4) Otvoriti browser i u search baru ukucati: http://localhost:8082